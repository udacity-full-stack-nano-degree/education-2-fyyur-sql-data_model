This is the public repository for Udacity's Full-Stack Nanodegree program.

I am using the underlying repository as foundation to develop my own version of the fyyur project.


The original project description can be found here: https://github.com/udacity/FSND/tree/master/projects/01_fyyur/starter_code
